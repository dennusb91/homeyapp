
const Homey = require('homey');
var request = require('request');
var needle = require('needle');
var fs = require('fs');


function fireTrigger(status){
	if(status == "on"){
		let rainStartTrigger = new Homey.FlowCardTrigger('alarm_on');
		rainStartTrigger
		    .register()
		    .trigger()
		        .catch( this.error )
		        .then( this.log )
	}else{
		let rainStartTrigger = new Homey.FlowCardTrigger('alarm_off');
		rainStartTrigger
		    .register()
		    .trigger()
		        .catch( this.error )
		        .then( this.log )
	}
}

class MyApp extends Homey.App {
	
	onInit() {
		let rainStartTrigger = new Homey.FlowCardTrigger('alarm_on');
		this.log('MyAlarm is running and ready...');
		var apiKey = Homey.ManagerSettings.get("apikey");
		
		var minutes = 1, the_interval = minutes*30*1000;
		setInterval(function(){
			var options = {
				headers: { 'API_KEY': apiKey }
			}
		needle.post('http://www.homeyalarm.com/getState', {foo:'bar'}, options,
		    function(err, resp, body){
		    	console.log(body);
		    	var alarmStateLocal = Homey.ManagerSettings.get("alarmStateLocal");
		    	var results = JSON.parse(body);

		    	//Check for difference
		    	if (results['State'] == "On" && alarmStateLocal == "Off"){
		    		var changed = true;
		    	}else if (results['State'] == "Off" && alarmStateLocal == "On"){
		    		var changed = true;
		    	}else if (alarmStateLocal == null) {
		    		var changed = true;
		    	}else{
		    		var changed = false;
		    	
		    	}

		        if (changed == true){
		        	console.log("Alarm state changed. Saving!");
		        	Homey.ManagerSettings.set("alarmStateLocal", results['State']);
		        	if (results['State'] == "On"){
		        		console.log("Alarm turned on! Fire trigger");
		        		fireTrigger("on");
		        	}else if(results['State'] == "Off"){
		        		console.log("Alarm turned off! Fire trigger");
		        		fireTrigger("off");
		        	}
		        }else{
		        	console.log("State didn't change" + alarmStateLocal);
		        }
		});
		}, the_interval);

	}
	
}

module.exports = MyApp;
